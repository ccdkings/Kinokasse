﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kinokasse.contracts;

namespace kinokasse.logic.tests
{
    [TestClass]
    public class UnitTestSaalpläne
    {
        public List<Guid> ErzeugeGuids(int anzahl)
        {
            var guids = new List<Guid>();

            for (int i = 0; i < anzahl; i++)
            {
                guids.Add(Guid.NewGuid());
            }

            return guids;
        }

        public IEnumerable<Film> TestDaten(IList<Guid> guids)
        {
            /* Film 1, 3 Vorstellungen */
            var film = new Film();
            film.Id = guids[0];
            film.Name = "Der Name der Rose";
            var vorstellungen = new List<Vorstellung>();
            film.Vorstellungen = vorstellungen;
            vorstellungen.Add(
                new Vorstellung
                {
                    Id = guids[1],
                    Preis = 7,
                    Saal = "Saal 1",
                    Tag = DayOfWeek.Monday,
                    Uhrzeit = new DateTime(1990, 1, 1, 16, 00, 00)
                }
                );

            vorstellungen.Add(
                new Vorstellung
                {
                    Id = guids[2],
                    Preis = 7,
                    Saal = "Saal 1",
                    Tag = DayOfWeek.Monday,
                    Uhrzeit = new DateTime(1990, 1, 1, 20, 00, 00)
                }
                );

            vorstellungen.Add(
                new Vorstellung
                {
                    Id = guids[3],
                    Preis = 7,
                    Saal = "Saal 1",
                    Tag = DayOfWeek.Tuesday,
                    Uhrzeit = new DateTime(1990, 1, 1, 20, 00, 00)
                }
                );

            yield return film;

            /* Film 2, 2 Vorstellungen */

            film = new Film();
            film.Id = guids[4];
            film.Name = "Vom Winde verweht";
            vorstellungen = new List<Vorstellung>();
            film.Vorstellungen = vorstellungen;
            vorstellungen.Add(
                new Vorstellung
                {
                    Id = guids[5],
                    Preis = 7,
                    Saal = "Saal 2",
                    Tag = DayOfWeek.Monday,
                    Uhrzeit = new DateTime(1990, 1, 1, 16, 00, 00)
                }
                );

            vorstellungen.Add(
                new Vorstellung
                {
                    Id = guids[6],
                    Preis = 7,
                    Saal = "Saal 2",
                    Tag = DayOfWeek.Monday,
                    Uhrzeit = new DateTime(1990, 1, 1, 20, 00, 00)
                }
                );

            yield return film;
        }

        [TestMethod]
        public void TestFindeVorstellungSuccess()
        {
            // arrange
            var saalpläne = new Saalpläne();
            var guids = ErzeugeGuids(20);
            var filme = TestDaten(guids);

            // act
            var vorstellung = saalpläne.FindeVorstellung(filme, guids[5]);

            // assert
            Assert.IsNotNull(vorstellung);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestFindeVorstellungFail()
        {
            // arrange
            var saalpläne = new Saalpläne();
            var guids = ErzeugeGuids(20);
            var filme = TestDaten(guids);

            // act
            var vorstellung1 = saalpläne.FindeVorstellung(filme, guids[4]);

            // assert
            // ExpectedException
        }

        [TestMethod]
        public void TestErmittleSaalname()
        {
            // arrange
            var saalpläne = new Saalpläne();
            var vorstellung = new Vorstellung
            {
                Saal = "Saal 2",
            };

            // act
            var saalname = saalpläne.ErmittleSaalname(vorstellung);

            // assert
            Assert.AreEqual(saalname, "Saal 2");
        }

        [TestMethod]
        public void TestErzeugePlätze()
        {
            // arrange
            var saalpläne = new Saalpläne();
            string[] zeilen = {" X ", "XX "};

            // act
            var plätze = saalpläne.ErzeugePlätze(zeilen);
            int i = 0;
            var plätzeArray = plätze.ToArray();

            // assert
            Assert.AreEqual(plätzeArray.Length, 3);
            Assert.AreEqual(plätzeArray[1].Status, Status.Frei);
            Assert.AreEqual(plätzeArray[2].Id, 2);
        }
    }
}