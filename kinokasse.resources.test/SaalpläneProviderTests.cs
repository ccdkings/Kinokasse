﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using kinokasse.resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kinokasse.resources.Tests
{
    [TestClass()]
    public class SaalpläneProviderTests
    {
        [TestMethod()]
        public void LeseSaalplanTest()
        {
            string[] expected = new []{ "    XXXXXXXXXXXXX",
                                        "  XXXXXXXXXXXXXXXXX",
                                        " XXXXXXXXXXXXXXXXXXX",
                                        "XXXXXXXXXXXXXXXXXXXXX",
                                        "XXXXXXXXXXXXXXXXXXXXX",
                                        "XXXXXXXXXXXXXXXXXXXXX"};
            var provider = new SaalpläneProvider();
            var result = provider.LeseSaalplan("Saal 1").ToArray();

            Assert.AreEqual(result[0], expected[0]);
            Assert.AreEqual(result[1], expected[1]);
            Assert.AreEqual(result[2], expected[2]);
            Assert.AreEqual(result[3], expected[3]);
            Assert.AreEqual(result[4], expected[4]);
            Assert.AreEqual(result[5], expected[5]);
        }
    }
}