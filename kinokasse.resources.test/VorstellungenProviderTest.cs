﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace kinokasse.resources.test
{
    [TestClass]
    public class VorstellungenProviderTest
    {
        [TestMethod]
        public void TestVorstellungenInFilmeEinfügen()
        {
            var provider = new VorstellungenProvider();
            var lines = provider.VorstellungenLaden().ToList();
            Assert.AreEqual(3, lines.Count());
            var filmeOhneVorstellungen = provider.FilmeErstellen(lines).ToList();
            Assert.AreEqual(3, filmeOhneVorstellungen.Count());

            var filmeMitVorstellungen = provider.VorstellungenInFilmeEinfügen(filmeOhneVorstellungen).ToList();
            Assert.AreEqual(3, filmeMitVorstellungen.Count());
            Assert.IsTrue(filmeMitVorstellungen.FirstOrDefault((a) => a.Name.Equals("Der Schuh des Königs")) != null,
                "Der Schuh des Königs not found");

            Assert.IsTrue(filmeMitVorstellungen.First((a) => a.Name.Equals("Der Schuh des Königs")).Vorstellungen.FirstOrDefault( (b)=>b.Saal.Equals("Saal 3") && b.Tag== DayOfWeek.Wednesday && b.Uhrzeit.Hour == 16)!= null,"Vorstellung not found");

        }
    }
}
