﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using kinokasse.ui;

namespace kinokasse
{
    class Program
    {
        static void Main(string[] args)
        {
			var interactor = new Interactor();

			var filme = interactor.Start();

			var kinoprogrammDialog = new KinoprogrammDialog();
			var belegungsplanDialog = new BelegungsplanDialog();
			kinoprogrammDialog.VorstellungGewählt += (filmId, vorstellungId) =>
			{
				var plätze = interactor.AuswahlVorstellung(filmId, vorstellungId);
				belegungsplanDialog.Start(plätze);
			};

			kinoprogrammDialog.Start(filme);
		}
    }
}
