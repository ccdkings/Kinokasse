﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using kinokasse.contracts;
using kinokasse.logic;
using kinokasse.resources;

namespace kinokasse
{
	public class Interactor
	{
		private readonly State<IList<Film>> _filmState = new State<IList<Film>>();

	
		public IEnumerable<Film> Start()
		{
			var vorstellungenProvider = new VorstellungenProvider();

			var zeilen = vorstellungenProvider.VorstellungenLaden();
			var filmeUndVorstellungen = vorstellungenProvider.FilmeErstellen(zeilen);
			var filme = vorstellungenProvider.VorstellungenInFilmeEinfügen(filmeUndVorstellungen).ToList();

			_filmState.Set(filme);
			return filme;
		}

		public IEnumerable<Platz> AuswahlVorstellung(Guid filmId, Guid vorstellungId)
		{
			var filme = _filmState.Get();
			var saalpläne = new Saalpläne();
			var saalpläneProvider = new SaalpläneProvider();

			var vorstellung = saalpläne.FindeVorstellung(filme, vorstellungId);
			var saalname = saalpläne.ErmittleSaalname(vorstellung);
			var saalplan = saalpläneProvider.LeseSaalplan(saalname);

			return saalpläne.ErzeugePlätze(saalplan);
		}
	}
}
