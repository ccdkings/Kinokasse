﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using kinokasse.contracts;

namespace kinokasse.resources
{
    public class VorstellungenProvider : IVorstellungenProvider
    {
        private static string[] _days = new string[] { "So", "Mo", "Di", "Mi", "Do", "Fr", "Sa" };
        private const string VorstellungenDatei = @".\Data\Vorstellungen.txt";

        public IEnumerable<string> VorstellungenLaden()
        {
            return File.ReadLines(VorstellungenDatei);
        }

        public IEnumerable<Tuple<Film, string>> FilmeErstellen(IEnumerable<string> zeilen)
        {
            foreach (var zeile in zeilen)
            {
                var filmInformation = zeile.Split(';');
                if (filmInformation.Length != 2)
                {
                    throw new InvalidDataException("Invalid Film-Vorstellungsdefinition: " + zeile);
                }
                yield return new Tuple<Film, string>(new Film() { Id = Guid.NewGuid(), Name = filmInformation [0]}, filmInformation[1] );
            }

        }

        public IEnumerable<Film> VorstellungenInFilmeEinfügen(IEnumerable<Tuple<Film, string>> filmeUndVorstellungen)
        {
            foreach (var filmUndVorstellungen in filmeUndVorstellungen)
            {
                var film = filmUndVorstellungen.Item1;
                var vorstellungen = new List<Vorstellung>();
                var vorstellungenSpits = filmUndVorstellungen.Item2.Split(',');

                foreach (var vorstellungSplit in vorstellungenSpits )
                {
                    string saal;
                    DayOfWeek dayOfWeek;
                    decimal preis;
                    DateTime uhrzeit;

                    ExtractVorstellungsData(out saal, vorstellungSplit, out dayOfWeek, out preis, out uhrzeit);

                    vorstellungen.Add(new Vorstellung()
                    {
                        Id = Guid.NewGuid(),
                        Preis = preis,
                        Tag = dayOfWeek,
                        Saal = saal,
                        Uhrzeit = uhrzeit
                    });
                }
                film.Vorstellungen = vorstellungen;
                yield return film;
            }
        }

        private void ExtractVorstellungsData(out string saal, string vorstellungSplit, out DayOfWeek dayOfWeek,
            out decimal preis, out DateTime uhrzeit)
        {
            var dataSplit = vorstellungSplit.Split('/');

            if (dataSplit.Length != 4)
            {
                throw new InvalidDataException("Invalid Vorstellungsdefinition: " + vorstellungSplit);
            }
            var day = dataSplit[0];
            var timeOfDay = dataSplit[1];

            saal = dataSplit[2];
            var preisString = dataSplit[3];

            dayOfWeek = GetDayOfWeek(day);
            preis = decimal.Parse(preisString, CultureInfo.InvariantCulture);

            uhrzeit = DateTime.Parse(string.Format("01.01.0001 {0}", timeOfDay), CultureInfo.CurrentCulture);
        }

        private DayOfWeek GetDayOfWeek(string day)
        {
            for (var ii = 0; ii < _days.Length; ii++)
            {
                if (day.Equals(_days[ii]))
                {
                    return (DayOfWeek) ii;
                }
            }
            throw new InvalidDataException("Unknown Day " + day);
        }
    }
}
