﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using kinokasse.contracts;

namespace kinokasse.resources
{
    public class SaalpläneProvider : ISaalpläneProvider
    {
        public IEnumerable<string> LeseSaalplan(string saalName)
        {
            string filename = Path.Combine("Data", Path.ChangeExtension(saalName, "txt"));
            return File.ReadLines(filename);
        }
    }
}
