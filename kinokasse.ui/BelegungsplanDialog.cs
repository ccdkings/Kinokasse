﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using kinokasse.contracts;

namespace kinokasse.ui
{
    public class BelegungsplanDialog : IBelegungsplanDialog
    {
        public void Start(IEnumerable<Platz> plätze)
        {
            BelegungsplanDarstellen(plätze);

            bool auswahlBeendet = false;
            while (!auswahlBeendet)
            {
                var input = Console.ReadLine();
                if (input == string.Empty)
                {
                    auswahlBeendet = true;
                }
                else
                {
                    var platzId = Convert.ToInt32(input);
                    //prüfen, ob Platz verändert werden darf
                    var patz = plätze.Where(x => x.Id == platzId).Single();
                    patz.Status = (patz.Status == Status.Frei) ? Status.Reserviert : Status.Frei;
                    BelegungsplanDarstellen(plätze);
                    //belegungsplan anpassen und neu darstellen
                }
            }
        }

        private static void BelegungsplanDarstellen(IEnumerable<Platz> plätze)
        {
            foreach (var platz in plätze)
            {
                Console.WriteLine(platz.Id + " " + platz.Status);
            }
            Console.WriteLine("Wählen Sie den gewünschten Sitzplatz.\n Eine leere Eingabe beendet die Auswahl.");
        }
    }
}
