﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using kinokasse.contracts;

namespace kinokasse.ui
{
    public class KinoprogrammDialog : IKinoprogrammDialog
    {
        public void Start(IEnumerable<Film> filme)
        {
            var displayDict = VorstellungenAuflisten(filme);

            var vorstellungIds = LeseKundenEingabe(displayDict);

            VorstellungGewählt(vorstellungIds.Item1, vorstellungIds.Item2);
        }

        private static Tuple<Guid, Guid> LeseKundenEingabe(Dictionary<int, Tuple<Guid, Guid>> displayDict)
        {
            string input = Console.ReadLine();
            var vorstellungId = Convert.ToInt32(input);
            Tuple<Guid, Guid> vorstellungIds;
            displayDict.TryGetValue(vorstellungId, out vorstellungIds);
            return vorstellungIds;
        }

        private static Dictionary<int, Tuple<Guid,Guid>> VorstellungenAuflisten(IEnumerable<Film> filme)
        {
            Dictionary<int, Tuple<Guid, Guid>> displayDictionary = new Dictionary<int, Tuple<Guid, Guid>>();
            int i = 0;
            foreach (var film in filme)
            {
                Console.WriteLine(film.Name);
                foreach (var vorstellung in film.Vorstellungen)
                {
                    displayDictionary.Add(i, new Tuple<Guid, Guid>(film.Id, vorstellung.Id));
                    Console.WriteLine(i.ToString() + " " + vorstellung.Uhrzeit.ToString() + " " + vorstellung.Tag);
                    i++;
                }


            }
            Console.WriteLine("Bitte geben Sie die Nummer der gewünschten Vorstellung ein\n\n\n");
            return displayDictionary;
        }

        public event Action<Guid, Guid> VorstellungGewählt;
    }
}
