﻿using System;
using System.Collections.Generic;
using kinokasse.contracts;

namespace kinokasse.logic
{
    public class Saalpläne : ISaalpläne
    {
        public Vorstellung FindeVorstellung(IEnumerable<Film> filme, Guid vorstellungId)
        {
            foreach (var film in filme)
            {
                foreach (var vorstellung in film.Vorstellungen)
                {
                    if (vorstellung.Id == vorstellungId)
                    {
                        return vorstellung;
                    }
                }
            }
            throw new ArgumentException("Vorstellung nicht gefunden");
        }

        public string ErmittleSaalname(Vorstellung vorstellung)
        {
            return vorstellung.Saal;
        }

        public IEnumerable<Platz> ErzeugePlätze(IEnumerable<string> zeilen)
        {
            int anzahlPlätze = 0;

            foreach (var zeile in zeilen)
            {
                var trimmedZeile = zeile.Trim();
                anzahlPlätze += trimmedZeile.Length;
            }

            for (int i = 0; i < anzahlPlätze; i++)
            {
                Platz platz = new Platz();
                platz.Id = i;
                platz.Status = Status.Frei;
                yield return platz;
            }
        }
    }
}