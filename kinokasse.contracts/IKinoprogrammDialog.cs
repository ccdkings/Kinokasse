﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace kinokasse.contracts
{
    public interface IKinoprogrammDialog
    {
        void Start(IEnumerable<Film> filme);
        event Action<Guid, Guid> VorstellungGewählt;
    }
}