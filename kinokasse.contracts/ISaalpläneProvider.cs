﻿using System.Collections.Generic;

namespace kinokasse.contracts
{
    public interface ISaalpläneProvider
    {
        IEnumerable<string> LeseSaalplan(string saalName);
    }
}