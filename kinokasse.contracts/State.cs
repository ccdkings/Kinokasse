﻿namespace kinokasse.contracts
{
    public class State<T>
    {
        private T _t;
        public T Get()
        {
            return _t;
        }

        public void Set(T t)
        {
           _t = t; 
        }
    }
}