﻿using System.Collections.Generic;

namespace kinokasse.contracts
{
    public interface IBelegungsplanDialog
    {
        void Start(IEnumerable<Platz> plätze);
    }
}