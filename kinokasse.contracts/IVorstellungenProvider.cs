﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace kinokasse.contracts
{
    public interface IVorstellungenProvider
    {
        IEnumerable<string> VorstellungenLaden();
        IEnumerable<Tuple<Film, string>> FilmeErstellen(IEnumerable<string> zeilen);
        IEnumerable<Film> VorstellungenInFilmeEinfügen(IEnumerable<Tuple<Film, string>> filmeUndVorstellungen);
    }
}