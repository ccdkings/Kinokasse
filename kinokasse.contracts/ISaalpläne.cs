﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace kinokasse.contracts
{
    public interface ISaalpläne
    {
        Vorstellung FindeVorstellung(IEnumerable<Film> filme, Guid vorstellungId);
        string ErmittleSaalname(Vorstellung vorstellung);
        IEnumerable<Platz> ErzeugePlätze(IEnumerable<string> zeilen);
    }
}