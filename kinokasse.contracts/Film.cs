﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace kinokasse.contracts
{
    public class Film
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
        public IEnumerable<Vorstellung> Vorstellungen { get; set; }
    }
}