﻿using System;
using System.Security.AccessControl;

namespace kinokasse.contracts
{
    public class Vorstellung
    {
        public DateTime Uhrzeit { get; set; }
        public DayOfWeek Tag { get; set; }
        public decimal Preis { get; set; }
        public string Saal { get; set; }
        public Guid Id { get; set; }

    }
}