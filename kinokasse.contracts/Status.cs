﻿namespace kinokasse.contracts
{
    public enum Status
    {
        Frei,
        Belegt,
        Reserviert
    }
}